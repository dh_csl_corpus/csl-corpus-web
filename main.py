import sqlite3
from flask import Flask, render_template, request
from flask import g

DATABASE = 'data/db.sqlite3'
app = Flask(__name__)

@app.route ('/')
def list():
    con = sqlite3.connect(DATABASE)
    con.row_factory = sqlite3.Row
    cur = con.cursor()
    cur.execute('select * from textblock where parent_block_id is NULL')
    rows = cur.fetchall()
    con.commit()
    con.close()
    return render_template('home.html',rows = rows)


@app.route ('/texts', methods=['GET', 'POST'])
def texts():
    con = sqlite3.connect(DATABASE)
    con.row_factory = sqlite3.Row
    cur = con.cursor()
    cur.execute('select * from textblock where parent_block_id is NULL')
    rows = cur.fetchall()
    con.commit()
    con.close()
    return render_template('texts.html',rows = rows)


@app.route ('/gloss')
def gloss():
    return render_template('glossary.html')

@app.route ('/project')
def project():
    return render_template('project.html')

@app.route ('/about')
def about():
    return render_template('about.html')

@app.route ('/contact')
def contact():
    return render_template('contact.html')

@app.route ('/view/<num>', methods=['GET', 'POST'])
def view(num):
    con = sqlite3.connect(DATABASE)
    con.row_factory = sqlite3.Row
    cur = con.cursor()
    cur.execute('select * from textblock where parent_block_id = (?)', (num,))
    rows = cur.fetchall()

    # breadcrumbs
    cur.execute('select * from textblock where id = (?)', (num,))
    current = cur.fetchall()
    name = current[0][1]
    parentid = current[0][6]
    curlist = [num, name, parentid]

    i = parentid

    crumbs = []

    while i:
        cur.execute('select * from textblock where id = (?)', (i,))
        pblock = cur.fetchall()

        if pblock:
            bid = pblock[0][0]
            name = pblock[0][1]
            pid = pblock[0][6]
            crumbs.append([bid, name, pid])
            i = pid

        else:
            break

    if crumbs:
        crumbs = sorted(crumbs, key=lambda x: x[0])

    if rows:
        return render_template('view.html', current =  curlist, crumbs = crumbs, rows = rows)

    if not rows:
        cur.execute('select * from paragraph where parent_block_id = (?)', (num,))
        pars = cur.fetchall()

        return render_template('view.html',pars = pars, current =  curlist, crumbs = crumbs, rows = rows)

    con.commit()
    con.close()

@app.route ('/hymns/<num>', methods=['GET', 'POST'])
def hymns(num):
    con = sqlite3.connect(DATABASE)
    con.row_factory = sqlite3.Row
    cur = con.cursor()
    cur.execute('select * from hymn where parent_id = (?)', (num,))
    hymns = cur.fetchall()

    # breadcrumbs
    cur.execute('select * from paragraph where id = (?)', (num,))
    par = cur.fetchall()
    txt = par[0][1]
    order = par[0][3]
    parentid = par[0][4]
    curlist = [num, txt, order, parentid]

    i = parentid

    crumbs = []

    while i:
        cur.execute('select * from textblock where id = (?)', (i,))
        pblock = cur.fetchall()

        if pblock:
            bid = pblock[0][0]
            name = pblock[0][1]
            pid = pblock[0][6]
            crumbs.append([bid, name, pid])
            i = pid

        else:
            break

    if crumbs:
        crumbs = sorted(crumbs, key=lambda x: x[0])



    con.commit()
    con.close()


    return render_template('hymns.html', current = curlist, crumbs = crumbs, hymns = hymns)

@app.route ('/hymn/<num>', methods=['GET', 'POST'])
def hymn(num):

    con = sqlite3.connect(DATABASE)
    con.row_factory = sqlite3.Row
    cur = con.cursor()

    # breadcrumbs
    crumbs = []
    i = num

    cur.execute('select * from hymn where id = (?)', (i,))
    hym = cur.fetchall()
    txt = hym[0][2]
    order = hym[0][6]
    parentid = hym[0][1]
    hlist = [i, txt, order, parentid]
    i = parentid

    cur.execute('select * from paragraph where id = (?)', (i,))
    par = cur.fetchall()
    txt = par[0][1]
    order = par[0][3]
    parentid = par[0][4]
    curlist = [i, txt, order, parentid]
    i = parentid

    while i:
        cur.execute('select * from textblock where id = (?)', (i,))
        pblock = cur.fetchall()
        if pblock:
            bid = pblock[0][0]
            name = pblock[0][1]
            pid = pblock[0][6]
            crumbs.append([bid, name, pid])
            i = pid

        else:
            break

    crumbs = sorted(crumbs, key=lambda x: x[0])


    cur.execute('select * from similarities where hymn1_id = (?)', (num,))
    sim = cur.fetchall()
    ids = [[x['similarity_type'], x['hymn2_id']] for x in sim]

    idsp = {}

    ids0 = []
    ids1 = []
    ids2 = []

    for id in ids:
        # breadcrumbs
        crumbs = []
        i = id[1]
        type = id[0]

        cur.execute('select * from hymn where id = (?)', (i,))
        hym = cur.fetchall()
        order = hym[0][6]
        txt = hym[0][2]
        parentid = hym[0][1]
        hym = [i, order, parentid, txt]
        i = parentid

        cur.execute('select * from paragraph where id = (?)', (i,))
        par = cur.fetchall()
        order = par[0][3]
        parentid = par[0][4]
        parag = [i, order, parentid]
        i = parentid

        while i:
            cur.execute('select * from textblock where id = (?)', (i,))
            pblock = cur.fetchall()
            if pblock:
                bid = pblock[0][0]
                name = pblock[0][1]
                pid = pblock[0][6]
                crumbs.append([bid, name, pid])
                i = pid

            else:
                break

        crumbs = sorted(crumbs, key=lambda x: x[0])

        if type == 0:
            ids0.append([id, crumbs, parag, hym])

        elif type == 1:
            ids1.append([id, crumbs, parag, hym])

        elif type == 2:
            ids2.append([id, crumbs, parag, hym])

        else:
            continue

        idsp = {1: ids0, 2: ids1, 3: ids2}

    con.commit()
    con.close()

    return render_template('hymn.html', hlist = hlist, current = curlist, crumbs = crumbs, idsp = idsp)



if __name__ == '__main__':
    app.run(debug=False)
    list()
